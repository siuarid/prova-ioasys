﻿using System;
using MovieScore.Domain.Bus;
using MovieScore.Domain.Events;
using MovieScore.Domain.Interfaces.Notifications;

namespace MovieScore.Infra.CrossCutting.Bus
{
    public class InMemoryBus : IBus
    {
        public static Func<IServiceProvider> ContainerAccessor { get; set; }
        private static IServiceProvider Container => ContainerAccessor();

        public void RaiseEvent<T>(T theEvent) where T : Event
        {
            Publish(theEvent);
        }

        private static void Publish<T>(T message) where T : Message
        {
            if (Container == null) return;

            var type = message.MessageType.Equals("DomainNotification")
                ? typeof(IDomainNotificationHandler<T>)
                : typeof(IHandler<T>);

            var obj = Container.GetService(type);

            ((IHandler<T>)obj)?.Handle(message);
        }
    }
}

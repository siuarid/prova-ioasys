﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieScore.Api.Controllers.Base;
using MovieScore.Application.Services.Interfaces;
using MovieScore.Application.ViewModels.Movie.Evaluation.Request;
using MovieScore.Application.ViewModels.Movie.Request;
using MovieScore.Domain.Bus;
using MovieScore.Domain.Interfaces.Notifications;
using MovieScore.Domain.Notifications;

namespace MovieScore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : BaseController
    {
        private readonly IMovieAppService _appService;

        public MovieController(IMovieAppService appService, IBus bus, IDomainNotificationHandler<DomainNotification> notifications) : base(bus, notifications)
        {
            _appService = appService;
        }

        [Produces("application/json")]
        [HttpPost]
        [Route("")]
        [Authorize(Roles = "Admin")]
        public IActionResult Insert([FromBody] MovieInsertViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                InvalidViewModelNotify();
                return Response();
            }

            _appService.Insert(viewModel);

            return Response();
        }


        [Produces("application/json")]
        [HttpPost]
        [Route("evaluation")]
        [Authorize(Roles = "CommonUser")]
        public IActionResult InsertEvaluation([FromBody] EvaluationInsertViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                InvalidViewModelNotify();
                return Response();
            }

            _appService.InsertEvaluation(viewModel);

            return Response();
        }


        [Produces("application/json")]
        [HttpGet]
        [Route("all")]
        [Authorize]
        public IActionResult GetAll()
        {
            var result = _appService.GetAll();

            return Response(result);
        }


        [Produces("application/json")]
        [HttpGet]
        [Route("")]
        [Authorize]
        public IActionResult Get(Guid id)
        {
            var result = _appService.Get(id);

            return Response(result);
        }

        [Produces("application/json")]
        [HttpGet]
        [Route("title")]
        [Authorize]
        public IActionResult GetByTitle(string title)
        {
            var result = _appService.GetByTitle(title);

            return Response(result);
        }
    }
}

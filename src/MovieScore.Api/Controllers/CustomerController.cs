﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieScore.Api.Controllers.Base;
using MovieScore.Application.Services.Interfaces;
using MovieScore.Application.ViewModels.Customer.Requests;
using MovieScore.Application.ViewModels.Customer.Responses;
using MovieScore.Domain.Bus;
using MovieScore.Domain.Interfaces.Notifications;
using MovieScore.Domain.Notifications;

namespace MovieScore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : BaseController
    {
        private readonly ICustomerAppService _appService;

        public CustomerController(ICustomerAppService appService, IBus bus, IDomainNotificationHandler<DomainNotification> notifications) : base(bus, notifications)
        {
            _appService = appService;
        }

        /// <summary>
        /// Inserts a new customer on database
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>User created to the customer</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(UserViewModel), 200)]
        [HttpPost]
        [Route("")]
        [AllowAnonymous]
        public IActionResult Insert([FromBody] CustomerInsertViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                InvalidViewModelNotify();
                return Response();
            }

            var result = _appService.Insert(viewModel);

            return Response(result);
        }

        /// <summary>
        /// Updates a customer's name and birth date
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>Customer updated</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(CustomerVIewModel), 200)]
        [HttpPut]
        [Route("")]
        [Authorize]
        public IActionResult Update([FromBody] CustomerUpdateViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                InvalidViewModelNotify();
                return Response();
            }

            var result = _appService.Update(viewModel);

            return Response(result);
        }

    }
}

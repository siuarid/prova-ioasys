﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieScore.Api.Controllers.Base;
using MovieScore.Application.Services.Interfaces;
using MovieScore.Application.ViewModels.User.Request;
using MovieScore.Application.ViewModels.User.Response;
using MovieScore.Domain.Bus;
using MovieScore.Domain.Interfaces.Notifications;
using MovieScore.Domain.Notifications;

namespace MovieScore.Api.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserAppService _appService;

        public UserController(IUserAppService appService, IBus bus, IDomainNotificationHandler<DomainNotification> notifications) : base(bus, notifications)
        {
            _appService = appService;
        }

        /// <summary>
        /// Inserts a new user on database
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>User created</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(UserInsertedVIewModel), 200)]
        [HttpPost]
        [Route("")]
        [Authorize(Roles = "Admin")]
        public IActionResult Insert([FromBody]UserInsertViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                InvalidViewModelNotify();
                return Response();
            }

            _appService.Insert(viewModel);

            return Response();
        }

        /// <summary>
        /// Updates a user on database
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>User updated</returns>
        [Produces("application/json")]
        [HttpPut]
        [Route("")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([FromBody] UserUpdateViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                InvalidViewModelNotify();
                return Response();
            }

            _appService.Update(viewModel);

            return Response();
        }

        /// <summary>
        /// Get all user who are not admin
        /// </summary>
        /// <returns>Users/returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(UserInsertedVIewModel), 200)]
        [HttpGet]
        [Route("common")]
        [Authorize(Roles = "Admin")]
        public IActionResult GetCommon(int? pageNumber = null, int? pageSize = null)
        {
            var result = _appService.GetCommon(pageNumber, pageSize);

            return Response(result);
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MovieScore.Api.Controllers.Base;
using MovieScore.Application.Services.Interfaces;
using MovieScore.Application.ViewModels.Auth;
using MovieScore.Domain.Bus;
using MovieScore.Domain.Interfaces.Notifications;
using MovieScore.Domain.Notifications;

namespace MovieScore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : BaseController
    {
        private readonly IAuthenticationAppService _authenticationAppService;
        private readonly IConfiguration _configuration;

        public AuthenticationController(IAuthenticationAppService authenticationAppService, IConfiguration configuration, IBus bus, IDomainNotificationHandler<DomainNotification> notifications) : base(bus, notifications)
        {
            _authenticationAppService = authenticationAppService;
            _configuration = configuration;
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login(LoginViewModel viewModel)
        {
            var value = _configuration.GetValue<string>("Secret");

            var result = _authenticationAppService.Login(viewModel, value);

            return Response(result);
        }
    }
}

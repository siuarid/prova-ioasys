﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MovieScore.Domain.Entities.Base;
using MovieScore.Domain.Interfaces.Repositories.Base;
using MovieScore.Infra.Data.Context;

namespace MovieScore.Infra.Data.Repositories.Base
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity<TEntity>
    {
        protected readonly MovieScoreContext _context;

        public Repository(MovieScoreContext context)
        {
            _context = context;
        }


        public virtual Guid Add(TEntity entity)
        {
            _context.Add(entity);

            return entity.Id;
        }

        public virtual TEntity GetById(Guid id, params Expression<Func<TEntity, object>>[] include)
        {
            Expression<Func<TEntity, bool>> predicate = x => x.Id == id;
            return Find(predicate, include).FirstOrDefault();
        }

        public virtual ICollection<TEntity> GetAll(params Expression<Func<TEntity, object>>[] include)
        {
            return Find(e => true, include);
        }

        public virtual ICollection<TEntity> Find(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] include)
        {
            if (predicate == null) return null;

           IQueryable<TEntity> query = _context.Set<TEntity>().AsNoTracking();

            if (include != null)
            {
                query = include.Aggregate(query, (current, item) => current.Include(item));
            }

            return query.Where(predicate).ToList();
        }

        public TEntity Get(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] include)
        {
            if (predicate == null) return null;

            IQueryable<TEntity> query = _context.Set<TEntity>();;

            if (include != null)
            {
                query = include.Aggregate(query, (current, item) => current.Include(item));
            }

            return query.Where(predicate).FirstOrDefault();
        }

        public virtual void Update(TEntity entity)
        {
            _context.Update(entity);
        }

        public virtual void Remove(Guid id)
        {
            Remove(GetById(id));
        }

        public virtual void Remove(TEntity entity)
        {
            _context.Remove(entity);
        }

        public virtual void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}

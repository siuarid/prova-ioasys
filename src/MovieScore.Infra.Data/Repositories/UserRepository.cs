﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Enums;
using MovieScore.Domain.Interfaces.Repositories;
using MovieScore.Infra.Data.Context;
using MovieScore.Infra.Data.Repositories.Base;

namespace MovieScore.Infra.Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(MovieScoreContext context) : base(context) { }

        public User GetActive(string login)
        {
            return _context.Set<User>().FirstOrDefault(user => user.Login == login);
        }

        public ICollection<User> GetCommon(int? pageNumber = null, int? pageSize = null)
        {
            var query = _context.Set<User>()
                .AsNoTracking()
                .Where(user => user.Active && user.Role == Role.CommonUser);

            if (pageNumber.HasValue && pageSize.HasValue)
            {
                query = query.Skip(pageSize.Value * (pageNumber.Value - 1)).Take(pageSize.Value);
            }

            return query.OrderBy(x => x.Login).ToList();
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieScore.Infra.Data.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "MovieScore");

            migrationBuilder.CreateTable(
                name: "Movie",
                schema: "MovieScore",
                columns: table => new
                {
                    movie_id = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: true),
                    RunningTime = table.Column<int>(type: "integer", nullable: false),
                    Director = table.Column<string>(type: "text", nullable: true),
                    Gender = table.Column<int>(type: "integer", nullable: false),
                    ProductionCompany = table.Column<string>(type: "text", nullable: true),
                    ReleaseDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CascadeMode = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.movie_id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                schema: "MovieScore",
                columns: table => new
                {
                    user_id = table.Column<Guid>(type: "uuid", nullable: false),
                    Login = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true),
                    Email = table.Column<string>(type: "text", nullable: true),
                    Active = table.Column<bool>(type: "boolean", nullable: false),
                    role_id = table.Column<int>(type: "integer", nullable: false),
                    CascadeMode = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.user_id);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                schema: "MovieScore",
                columns: table => new
                {
                    customer_id = table.Column<Guid>(type: "uuid", nullable: false),
                    user_id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    BirthDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CascadeMode = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.customer_id);
                    table.ForeignKey(
                        name: "FK_Customers_User_user_id",
                        column: x => x.user_id,
                        principalSchema: "MovieScore",
                        principalTable: "User",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Evaluation",
                schema: "MovieScore",
                columns: table => new
                {
                    evaluation_id = table.Column<Guid>(type: "uuid", nullable: false),
                    customer_id = table.Column<Guid>(type: "uuid", nullable: false),
                    movie_id = table.Column<Guid>(type: "uuid", nullable: false),
                    Rate = table.Column<int>(type: "integer", nullable: false),
                    EvaluationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: true),
                    CascadeMode = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Evaluation", x => x.evaluation_id);
                    table.ForeignKey(
                        name: "FK_Evaluation_Customers_customer_id",
                        column: x => x.customer_id,
                        principalSchema: "MovieScore",
                        principalTable: "Customers",
                        principalColumn: "customer_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Evaluation_Movie_movie_id",
                        column: x => x.movie_id,
                        principalSchema: "MovieScore",
                        principalTable: "Movie",
                        principalColumn: "movie_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Evaluation_User_UserId",
                        column: x => x.UserId,
                        principalSchema: "MovieScore",
                        principalTable: "User",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Customers_user_id",
                schema: "MovieScore",
                table: "Customers",
                column: "user_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Evaluation_customer_id",
                schema: "MovieScore",
                table: "Evaluation",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_Evaluation_movie_id",
                schema: "MovieScore",
                table: "Evaluation",
                column: "movie_id");

            migrationBuilder.CreateIndex(
                name: "IX_Evaluation_UserId",
                schema: "MovieScore",
                table: "Evaluation",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Evaluation",
                schema: "MovieScore");

            migrationBuilder.DropTable(
                name: "Customers",
                schema: "MovieScore");

            migrationBuilder.DropTable(
                name: "Movie",
                schema: "MovieScore");

            migrationBuilder.DropTable(
                name: "User",
                schema: "MovieScore");
        }
    }
}

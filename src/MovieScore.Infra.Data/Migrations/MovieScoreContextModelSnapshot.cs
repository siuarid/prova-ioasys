﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MovieScore.Infra.Data.Context;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace MovieScore.Infra.Data.Migrations
{
    [DbContext(typeof(MovieScoreContext))]
    partial class MovieScoreContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasDefaultSchema("MovieScore")
                .UseIdentityByDefaultColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.1");

            modelBuilder.Entity("MovieScore.Domain.Entities.Customer", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid")
                        .HasColumnName("customer_id");

                    b.Property<DateTime>("BirthDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("CascadeMode")
                        .HasColumnType("integer");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uuid")
                        .HasColumnName("user_id");

                    b.HasKey("Id");

                    b.HasIndex("UserId")
                        .IsUnique();

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("MovieScore.Domain.Entities.Evaluation", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid")
                        .HasColumnName("evaluation_id");

                    b.Property<int>("CascadeMode")
                        .HasColumnType("integer");

                    b.Property<Guid>("CustomerId")
                        .HasColumnType("uuid")
                        .HasColumnName("customer_id");

                    b.Property<DateTime>("EvaluationDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<Guid>("MovieId")
                        .HasColumnType("uuid")
                        .HasColumnName("movie_id");

                    b.Property<int>("Rate")
                        .HasColumnType("integer");

                    b.Property<Guid?>("UserId")
                        .HasColumnType("uuid");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.HasIndex("MovieId");

                    b.HasIndex("UserId");

                    b.ToTable("Evaluation");
                });

            modelBuilder.Entity("MovieScore.Domain.Entities.Movie", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid")
                        .HasColumnName("movie_id");

                    b.Property<int>("CascadeMode")
                        .HasColumnType("integer");

                    b.Property<string>("Director")
                        .HasColumnType("text");

                    b.Property<int>("Gender")
                        .HasColumnType("integer");

                    b.Property<string>("ProductionCompany")
                        .HasColumnType("text");

                    b.Property<DateTime>("ReleaseDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("RunningTime")
                        .HasColumnType("integer");

                    b.Property<string>("Title")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Movie");
                });

            modelBuilder.Entity("MovieScore.Domain.Entities.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid")
                        .HasColumnName("user_id");

                    b.Property<bool>("Active")
                        .HasColumnType("boolean");

                    b.Property<int>("CascadeMode")
                        .HasColumnType("integer");

                    b.Property<string>("Email")
                        .HasColumnType("text");

                    b.Property<string>("Login")
                        .HasColumnType("text");

                    b.Property<string>("Password")
                        .HasColumnType("text");

                    b.Property<int>("Role")
                        .HasColumnType("integer")
                        .HasColumnName("role_id");

                    b.HasKey("Id");

                    b.ToTable("User");
                });

            modelBuilder.Entity("MovieScore.Domain.Entities.Customer", b =>
                {
                    b.HasOne("MovieScore.Domain.Entities.User", "User")
                        .WithOne("Customer")
                        .HasForeignKey("MovieScore.Domain.Entities.Customer", "UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("User");
                });

            modelBuilder.Entity("MovieScore.Domain.Entities.Evaluation", b =>
                {
                    b.HasOne("MovieScore.Domain.Entities.Customer", "Customer")
                        .WithMany("Evaluations")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MovieScore.Domain.Entities.Movie", "Movie")
                        .WithMany("Evaluations")
                        .HasForeignKey("MovieId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MovieScore.Domain.Entities.User", null)
                        .WithMany("Evaluations")
                        .HasForeignKey("UserId");

                    b.Navigation("Customer");

                    b.Navigation("Movie");
                });

            modelBuilder.Entity("MovieScore.Domain.Entities.Customer", b =>
                {
                    b.Navigation("Evaluations");
                });

            modelBuilder.Entity("MovieScore.Domain.Entities.Movie", b =>
                {
                    b.Navigation("Evaluations");
                });

            modelBuilder.Entity("MovieScore.Domain.Entities.User", b =>
                {
                    b.Navigation("Customer");

                    b.Navigation("Evaluations");
                });
#pragma warning restore 612, 618
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieScore.Domain.Entities;

namespace MovieScore.Infra.Data.Mappings
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");

            builder.Property(x => x.Id)
                .HasColumnName("user_id");

            builder.Property(x => x.Role)
                .HasColumnName("role_id");

            builder.Property(x => x.Active);
            builder.Property(x => x.Login);
            builder.Property(x => x.Password);
        }
    }
}

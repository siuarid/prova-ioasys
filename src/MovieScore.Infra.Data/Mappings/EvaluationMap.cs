using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieScore.Domain.Entities;

namespace MovieScore.Infra.Data.Mappings
{
    public class EvaluationMap : IEntityTypeConfiguration<Evaluation>
    {
        public void Configure(EntityTypeBuilder<Evaluation> builder)
        {
            builder.ToTable("Evaluation");

            builder.Property(x => x.Id)
                .HasColumnName("evaluation_id");
            
            builder.Property(x => x.MovieId)
                .HasColumnName("movie_id");
            
            builder.Property(x => x.CustomerId)
                .HasColumnName("customer_id");
            
            builder.HasOne(x => x.Customer)
                .WithMany(x => x.Evaluations)
                .HasForeignKey(x => x.CustomerId);
            
            builder.HasOne(x => x.Movie)
                .WithMany(x => x.Evaluations)
                .HasForeignKey(x => x.MovieId);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using MovieScore.Domain.Entities;

namespace MovieScore.Domain.Interfaces.Services
{
    public interface IMovieService
    {
        void Insert(Movie movie);
        void InsertEvaluation(Guid movieId, int rate);
        ICollection<Movie> GetAll();
        Movie Get(Guid id);
        Movie GetByTitle(string title);
    }
}

﻿using System;
using MovieScore.Domain.Entities.Base;
using MovieScore.Domain.Enums;
using System.Collections.Generic;

namespace MovieScore.Domain.Entities
{
    public class Movie : Entity<Movie>
    {
        public string Title { get; private set; }
        public int RunningTime { get; private set; }
        public string Director { get; private set; }
        public MovieGender Gender { get; private set; }
        public string ProductionCompany { get; private set; }
        public DateTime ReleaseDate { get; private set; }

        public ICollection<Evaluation> Evaluations { get; private set; }
    }
}

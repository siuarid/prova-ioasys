﻿using System;
using System.Collections.Generic;
using FluentValidation.Results;
using MovieScore.Domain.Bus;
using MovieScore.Domain.Notifications;

namespace MovieScore.Domain.Services.Base
{
    public class ServiceBase
    {
        private readonly IBus _bus;

        protected ServiceBase(IBus bus)
        {
            _bus = bus;
        }

        protected void NotifyValidationError(ValidationResult validationResult)
        {
            foreach (var erro in validationResult.Errors)
            {
                Console.WriteLine(erro.ErrorMessage);
                _bus.RaiseEvent(new DomainNotification(erro.PropertyName, erro.ErrorMessage));
            }
        }

        protected void NotifyValidationError(string errorMessage)
        {
            var errors = new List<ValidationFailure> { new ValidationFailure(null, errorMessage) };

            var validationResult = new ValidationResult(errors);

            NotifyValidationError(validationResult);
        }
    }
}

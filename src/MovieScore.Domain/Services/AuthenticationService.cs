using MovieScore.Domain.Bus;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Repositories;
using MovieScore.Domain.Interfaces.Services;
using MovieScore.Domain.Services.Base;

namespace MovieScore.Domain.Services
{
    public class AuthenticationService : ServiceBase, IAuthenticationService
    {
        private readonly IUserRepository _userRepository;

        public AuthenticationService(IUserRepository userRepository, IBus bus) : base(bus) 
        {
            _userRepository = userRepository;
        }
        
        public User Authenticate(string login, string password)
        {
            var user = _userRepository.GetActive(login);

            if (user == null)
            {
                NotifyValidationError("Wrong email");
                return null;
            }

            if(user != null)
            {
                if(password != user.Password)
                {
                    NotifyValidationError("Wrong password");
                    return null;
                }
            }

            return user;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using MovieScore.Domain.Bus;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Repositories;
using MovieScore.Domain.Interfaces.Services;
using MovieScore.Domain.Services.Base;
using MovieScore.Infra.CrossCutting.Helpers;

namespace MovieScore.Domain.Services
{
    public class MovieService : ServiceBase, IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IEvaluationRepository _evaluationRepository;
        private readonly ITokenHelper _tokenHelper;
        private readonly ICustomerRepository _customerRepository;

        public MovieService(IMovieRepository movieRepository, IEvaluationRepository evaluationRepository, ICustomerRepository customerRepository, ITokenHelper tokenHelper, IBus bus) : base(bus)
        {
            _movieRepository = movieRepository;
            _evaluationRepository = evaluationRepository;
            _tokenHelper = tokenHelper;
            _customerRepository = customerRepository;
        }

        public Movie Get(Guid id)
        {
            return _movieRepository.GetById(id);
        }

        public ICollection<Movie> GetAll()
        {
            return _movieRepository.GetWithEvaluations();
        }

        public Movie GetByTitle(string title)
        {
            return _movieRepository.Get(x => x.Title.ToUpper() == title.ToUpper());
        }

        public void Insert(Movie movie)
        {
            _movieRepository.Add(movie);
            _movieRepository.Save();
        }

        public void InsertEvaluation(Guid movieId, int rate)
        {
            if(rate < 0 || rate > 4)
            {
                NotifyValidationError("Rate must be between 0 and 4");
                return;
            }

            if(_movieRepository.GetById(movieId) == null)
            {
                NotifyValidationError("Movie not found");
                return;
            }

            var customerId = _customerRepository.GetId(_tokenHelper.GetLoggedUserId());

            if(customerId == null)
            {
                NotifyValidationError("This user is not a costumer");
                return;
            }

            var evaluation = new Evaluation(customerId, movieId, rate);

            _evaluationRepository.Add(evaluation);
            _evaluationRepository.Save();
        }
    }
}

﻿namespace MovieScore.Domain.Enums
{
    public enum MovieGender
    {
        Horror,
        Terror,
        Action,
        Comedy,
        Drama,
        Kids,
        SciFi
    }
}

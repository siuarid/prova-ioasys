﻿using System;

namespace MovieScore.Domain.Commands
{
    public class CustomerUpdateCommand
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
    }
}

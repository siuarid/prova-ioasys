﻿using MovieScore.Application.Services.Interfaces;
using MovieScore.Application.ViewModels.Auth;
using MovieScore.Domain.Interfaces.Repositories;
using MovieScore.Domain.Interfaces.Services;
using MovieScore.Infra.CrossCutting.Helpers;

namespace MovieScore.Application.Services.Concretes
{
    public class AuthenticationAppService : IAuthenticationAppService
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ITokenHelper _tokenHelper;

        public AuthenticationAppService(IAuthenticationService authenticationService, ITokenHelper tokenHelper)
        {
            _authenticationService = authenticationService;
            _tokenHelper = tokenHelper;
        }

        public LoginResponseViewModel Login(LoginViewModel viewModel, string secret)
        {
            var user = _authenticationService.Authenticate(viewModel.Login, viewModel.Password);

            return new LoginResponseViewModel
            {
                Token = user == null ? string.Empty : _tokenHelper.GenerateToken(user.Id, user.Role.ToString(), secret)
            };
        }
    }
}

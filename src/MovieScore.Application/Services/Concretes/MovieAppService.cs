﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MovieScore.Application.Services.Concretes.Base;
using MovieScore.Application.Services.Interfaces;
using MovieScore.Application.ViewModels.Movie.Evaluation.Request;
using MovieScore.Application.ViewModels.Movie.Request;
using MovieScore.Application.ViewModels.Movie.Response;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Services;

namespace MovieScore.Application.Services.Concretes
{
    public class MovieAppService : AppServiceBase, IMovieAppService
    {
        private readonly IMovieService _movieService;

        public MovieAppService(IMovieService movieService, IMapper mapper) : base(mapper)
        {
            _movieService = movieService;
        }

        public MovieViewModel Get(Guid id)
        {
            return _mapper.Map<MovieViewModel>(_movieService.Get(id));
        }

        public ICollection<MovieViewModel> GetAll()
        {
            return _mapper.Map<ICollection<MovieViewModel>>(_movieService.GetAll());
        }

        public MovieViewModel GetByTitle(string title)
        {
            return _mapper.Map<MovieViewModel>(_movieService.GetByTitle(title));
        }

        public void Insert(MovieInsertViewModel viewModel)
        {
            var movie = _mapper.Map<Movie>(viewModel);

            _movieService.Insert(movie);
        }

        public void InsertEvaluation(EvaluationInsertViewModel viewModel)
        {
            _movieService.InsertEvaluation(viewModel.MovieId, viewModel.Rate);
        }
    }
}

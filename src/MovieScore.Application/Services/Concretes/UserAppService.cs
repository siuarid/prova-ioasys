﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MovieScore.Application.Services.Concretes.Base;
using MovieScore.Application.Services.Interfaces;
using MovieScore.Application.ViewModels.User.Request;
using MovieScore.Application.ViewModels.User.Response;
using MovieScore.Domain.Commands;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Services;

namespace MovieScore.Application.Services.Concretes
{
    public class UserAppService : AppServiceBase, IUserAppService
    {
        private readonly IUserService _userService;

        public UserAppService(IUserService userService, IMapper mapper) : base(mapper)
        {
            _userService = userService;
        }

        public ICollection<UserCommonViewModel> GetCommon(int? pageNumber = null, int? pageSize = null)
        {
            return _mapper.Map<ICollection<UserCommonViewModel>>(_userService.GetCommon(pageNumber, pageSize));
        }

        public void Insert(UserInsertViewModel viewModel)
        {
            _userService.Insert(_mapper.Map<User>(viewModel));
        }

        public void Update(UserUpdateViewModel viewModel)
        {
            _userService.Update(_mapper.Map<UserUpdateCommand>(viewModel));
        }
    }
}

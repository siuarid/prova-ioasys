﻿using MovieScore.Application.Services.Interfaces.Base;
using MovieScore.Application.ViewModels.Customer.Requests;
using MovieScore.Application.ViewModels.Customer.Responses;

namespace MovieScore.Application.Services.Interfaces
{
    public interface ICustomerAppService : IAppServiceBase
    {
        UserViewModel Insert(CustomerInsertViewModel viewModel);
        CustomerVIewModel Update(CustomerUpdateViewModel viewModel);
        
    }
}

﻿using AutoMapper;
using MovieScore.Application.ViewModels.Customer.Requests;
using MovieScore.Application.ViewModels.Movie.Request;
using MovieScore.Application.ViewModels.User.Request;
using MovieScore.Domain.Commands;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Enums;

namespace MovieScore.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<CustomerInsertViewModel, Customer>();
            CreateMap<CommonUserInsertVIewModel, User>()
                .ConstructUsing(x => new User(x.Login, x.Email, x.Password, Role.CommonUser));

            CreateMap<CustomerUpdateViewModel, CustomerUpdateCommand>();

            CreateMap<UserInsertViewModel, User>();
            CreateMap<UserUpdateViewModel, UserUpdateCommand>();

            CreateMap<MovieInsertViewModel, Movie>();
        }
    }
}

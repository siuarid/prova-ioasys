﻿using System.Linq;
using AutoMapper;
using MovieScore.Application.ViewModels.Customer.Responses;
using MovieScore.Application.ViewModels.Movie.Response;
using MovieScore.Application.ViewModels.User.Response;
using MovieScore.Domain.Entities;

namespace MovieScore.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Customer, CustomerVIewModel>();
            CreateMap<User, UserCommonViewModel>();
            CreateMap<User, UserViewModel>();

            CreateMap<Movie, MovieViewModel>()
                .ForMember(x => x.Gender, opt => opt.MapFrom(src => src.Gender.ToString()))
                .ForMember(x => x.Rate, opt => opt.MapFrom(src => src.Evaluations.Any() ?  src.Evaluations.Select(x => x.Rate).Average() : 0));
        }
    }
}

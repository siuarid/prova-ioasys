﻿namespace MovieScore.Application.ViewModels.User.Response
{
    public class UserCommonViewModel
    {
        public string Login { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
    }
}

﻿using System;

namespace MovieScore.Application.ViewModels.User.Response
{
    public class UserInsertedVIewModel
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
    }
}

﻿using System;
using MovieScore.Domain.Enums;

namespace MovieScore.Application.ViewModels.User.Request
{
    public class UserUpdateViewModel
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public Role Role { get; set; }
    }
}

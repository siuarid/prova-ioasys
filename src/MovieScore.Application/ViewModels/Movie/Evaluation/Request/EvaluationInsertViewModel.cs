﻿using System;

namespace MovieScore.Application.ViewModels.Movie.Evaluation.Request
{
    public class EvaluationInsertViewModel
    {
        public Guid MovieId { get; set; }
        public int Rate { get; set; }
    }
}

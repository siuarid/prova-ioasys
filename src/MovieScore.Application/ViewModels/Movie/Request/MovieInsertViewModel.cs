﻿using System;
using MovieScore.Domain.Enums;

namespace MovieScore.Application.ViewModels.Movie.Request
{
    public class MovieInsertViewModel
    {
        public string Title { get; set; }
        public int RunningTime { get; set; }
        public string Director { get; set; }
        public MovieGender Gender { get; set; }
        public string ProductionCompany { get; set; }
        public DateTime ReleaseDate { get; set; }

    }
}

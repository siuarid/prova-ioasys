﻿using System;

namespace MovieScore.Application.ViewModels.Customer.Requests
{
    public class CustomerUpdateViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
    }
}

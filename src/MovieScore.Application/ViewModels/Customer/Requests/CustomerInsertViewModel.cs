﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieScore.Application.ViewModels.Customer.Requests
{
    public class CustomerInsertViewModel
    {
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public CommonUserInsertVIewModel User { get; set; }
    }
}

﻿namespace MovieScore.Application.ViewModels.Auth
{
    public class LoginResponseViewModel
    {
        public string Token { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace MovieScore.Application.ViewModels.Auth
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Login is required")]
        public string Login { get; set; }
        [Required(ErrorMessage = "Password can't be empty")]
        public string Password { get; set; }
    }
}

using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;

namespace MovieScore.Infra.CrossCutting.Helpers
{
    public class TokenHelper : ITokenHelper
    {
        private readonly IHttpContextAccessor _context;

        public TokenHelper(IHttpContextAccessor context)
        {
            _context = context;
        }

        public string GenerateToken(Guid userId, string role, string secret)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("UserId", userId.ToString()),
                    new Claim(ClaimTypes.Role, role)
                }),
                Expires = DateTime.UtcNow.AddHours(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public Guid GetLoggedUserId()
        {
            var userId = _context.HttpContext.User.Claims.Where(x => x.Type == "UserId").FirstOrDefault();

            return Guid.Parse(userId.Value.ToString());
        }
    }
}
